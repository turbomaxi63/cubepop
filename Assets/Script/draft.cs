using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class draft : MonoBehaviour
{
    [SerializeField]
    private Transform myObj; //������������ ������ ������ �� �����
    //GameObject Figure = null; // �����.
    [SerializeField]
    private float distance = 10;
    private Transform cube;
    [SerializeField]
    private DOTweenAnimation DTPlay;
    private void Start()
    {
        DTPlay.DOPlay();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, distance))
            {
                if (hit.transform == transform)
                {
                    cube = (Transform)Instantiate(myObj, transform.position - new Vector3(0f, 0f, transform.localScale.z * 0.5f), transform.rotation);
                    cube.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z) * 0.5f;
                    if (cube.localScale.x < 6)
                    {
                        cube.localScale = new Vector3(6f, 6f, 6f);
                    }
                    cube = (Transform)Instantiate(myObj, transform.position + new Vector3(0f, 0f, transform.localScale.z * 0.5f), transform.rotation);
                    cube.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z) * 0.5f;
                    if (cube.localScale.x < 6)
                    {
                        cube.localScale = new Vector3(6f, 6f, 6f);
                    }
                    Destroy(gameObject);
                }
            }
        }
    }
    [SerializeField]
    private ParticleSystem crash;
    private ParticleSystem particleCrash;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Collision"))
        {
            Debug.Log("particl");
            other.transform.parent = null;
            particleCrash = Instantiate<ParticleSystem>(crash);
            particleCrash.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, -1);
            particleCrash.Play();
            Rigidbody rb = other.GetComponent<Rigidbody>();
            if (rb)
            {
                rb.isKinematic = false;

            }
            Destroy(other.gameObject);
        }
    }
}