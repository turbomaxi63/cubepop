using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MainScript : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private float timer = 5.0f; // ������ ������ �� 10 ������
    private Vector3 scale; // ���������� ��� �������� �������
    private Vector3 newscale; // ���������� ��� ������ �������
    private Transform cube;
   

    // Start is called before the first frame update
    void Start()
    {
        scale = new Vector3(player.transform.localScale.x, player.transform.localScale.y, player.transform.localScale.z); // ��������� ������� ������
        newscale = scale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
         if (cube.localScale.x >= 15)
        {
           timer -= 1.0f * Time.deltaTime; // �������� �� ������� 1 � �������
        player.transform.localScale = Vector3.Lerp(player.transform.localScale, newscale, Time.deltaTime);
        if (timer <= 0.0f)
        {
            newscale += new Vector3(1f, 1f, 1f); // ������ ����� ������
            timer = 5.0f; // ����� ������ ������ �� 10 ������
        }
            cube.localScale = new Vector3(15f, 15, 15);
        }
        

    }
}
